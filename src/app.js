import React from 'react'
import { createBrowserHistory } from 'history'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import AuthorizedRoute from './components/mixins/authorized-route'
import UnauthorizedRoute from './components/mixins/unauthorized-route'
import LayoutAuthenticated from './components/layouts/layout-authenticated'
import LoginView from './views/login'
import RegisterView from './views/register'
import Error404View from './views/error404'
import NavBar from './components/navbar'
import Preloader from './components/preloader'

const hist = createBrowserHistory()

class App extends React.Component {
    render () {
        const {
            global: { loading }
        } = this.props
        return (
            <Router history={hist}>
                <NavBar />
                {loading.length > 0 && <Preloader />}
                <Switch>
                    <UnauthorizedRoute path='/auth' component={LoginView} />
                    <UnauthorizedRoute path='/register' component={RegisterView} />
                    <AuthorizedRoute component={LayoutAuthenticated} />
                    <Route component={Error404View} />
                </Switch>
            </Router>
        )
    }
}

function mapStateToProps (state) {
    const { global } = state
    return {
        global
    }
}

export default connect(mapStateToProps)(App)
