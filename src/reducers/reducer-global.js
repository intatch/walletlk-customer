import {
    GLOBAL_ADD_LOADER,
    GLOBAL_REMOVE_LOADER,
    GLOBAL_SET_CUSTOMER_BANK_ACCOUNT,
    GLOBAL_SET_CUSTOMER_CREDIT_ACCOUNT,
    GLOBAL_SET_SYSTEM_BANK_ACCOUNT
} from '../utils/index'

const initState = {
    loading: [],
    systemBankAccounts: [],
    customerBankAccounts: [],
    customerCreditAccounts: []
}

export default function globalReducer (state = initState, action) {
    switch (action.type) {
    case GLOBAL_ADD_LOADER:
        return {
            ...state,
            loading: [ ...state.loading, action.payload ]
        }
    case GLOBAL_REMOVE_LOADER:
        return {
            ...state,
            loading: state.loading.filter(item => item !== action.payload)
        }
    case GLOBAL_SET_SYSTEM_BANK_ACCOUNT:
        return {
            ...state,
            systemBankAccounts: action.payload
        }
    case GLOBAL_SET_CUSTOMER_BANK_ACCOUNT:
        return {
            ...state,
            customerBankAccounts: action.payload
        }
    case GLOBAL_SET_CUSTOMER_CREDIT_ACCOUNT:
        return {
            ...state,
            customerCreditAccounts: action.payload
        }
    }
    return state
}
