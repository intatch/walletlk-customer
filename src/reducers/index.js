import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import authReducer from './reducer-authentication'
import messageReducer from './reducer-message'
import globalReducer from './reducer-global'

const rootReducer = combineReducers({
    form: formReducer,
    global: globalReducer,
    auth: authReducer,
    message: messageReducer
})

export default rootReducer
