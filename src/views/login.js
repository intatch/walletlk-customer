import React, { Component } from 'react'
import { connect } from 'react-redux'
import { required } from 'redux-form-validators'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router-dom'

import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'

import { authLogin } from '../actions/authentication'
import InputField from '../components/forms/form-input'
import { withStyles } from '@material-ui/core'

const styles = theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
})

class LoginView extends Component {
    submitForm = (values) => {
        this.props.authLogin(values, this.props.history)
    }

    render () {
        const {
            classes,
            pristine,
            submitting,
            handleSubmit,
            authMessage
        } = this.props

        return (
            <Container component='main' maxWidth='xs'>
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component='h1' variant='h5'>เข้าสู่ระบบ</Typography>
                    <form className={classes.form} onSubmit={handleSubmit(this.submitForm)}>
                        <Field component={InputField} label='username' name='username' validate={[required()]} />
                        <Field component={InputField} label='รหัสผ่าน' name='password' type='password' validate={required()} />
                        {authMessage && <div className='text-danger'>{authMessage.message}</div>}
                        <Grid container spacing={2}>
                            <Grid item xs>
                                <Button type='submit' fullWidth variant='contained' color='primary' className={classes.submit} disabled={pristine || submitting}>
                                    เข้าสู่ระบบ
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
                <Box mt={5}>
                    คุณจะต้องเป็นสมาชิกของ walletLk.COM ก่อน จึงจะสามารถเข้าสู่ระบบได้ เป็นสมาชิกกับเราหรือยังค่ะ?
                    <Link to='/register'>สมัครสมาชิก</Link>
                </Box>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        authMessage: state.message.authMessage
    }
}

const reduxFormLoginView = reduxForm({
    form: 'login',
    enableReinitialize: true
})(LoginView)

export default connect(mapStateToProps, { authLogin })(withStyles(styles)(reduxFormLoginView))
