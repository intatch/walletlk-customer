import React, { Component } from 'react'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'

import client from '../utils/client'
import {
    API_PROMOTION_URL,
    API_URL
} from '../utils/urls'

class PromotionView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            promotions: []
        }
    }

    componentDidMount () {
        this.fetchPromotions()
    }

    fetchPromotions = () => {
        client.get(API_PROMOTION_URL).then(res => {
            this.setState({
                promotions: res.data
            })
        })
    }

    renderPromotions = () => {
        return this.state.promotions.map((item, index) => {
            return (
                <Grid className='p-3' xs={11} key={index}>
                    <Card>
                        {item.image && <CardMedia
                            component="img"
                            image={`${API_URL}${item.image}`}
                            title={`${item.name}`}
                        />}
                        <CardContent>
                            <Typography color='primary' component='h1' variant='h5'>{item.name}</Typography>
                            <div dangerouslySetInnerHTML={{ __html: item.detail }} />
                        </CardContent>
                    </Card>
                </Grid>
            )
        })
    }

    render () {
        return (
            <Grid container justify='center'>
                {this.renderPromotions()}
            </Grid>
        )
    }
}

export default PromotionView
