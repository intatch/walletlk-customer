import React, { Component } from 'react'
import { Field, reduxForm, FieldArray } from 'redux-form'
import { required, email, length } from 'redux-form-validators'
import Swal from 'sweetalert2'

import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import PersonOutlineIcon from '@material-ui/icons/PersonOutline'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import ClearIcon from '@material-ui/icons/Clear'
import axios from 'axios'

import SelectField from '../components/forms/form-select'
import InputField from '../components/forms/form-input'
import {
    API_BANK_URL,
    API_REGISTER_URL,
    API_URL
} from '../utils/urls'
import { getBankIcon } from '../utils/helpers'
import { withStyles } from '@material-ui/core'

const styles = theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
})

class RegisterView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            banks: []
        }
    }

    componentDidMount () {
        this.fetchBanks()
    }

    fetchBanks = () => {
        axios.get(`${API_URL}${API_BANK_URL}`).then(res => {
            this.setState({
                banks: res.data
            })
        })
    }

    resetForm = () => {
        this.props.reset()
        this.props.history.push('/auth')
    }

    submitForm = (value) => {
        if (value.promotions) {
            value.promotions = [value.promotions]
        }

        axios.post(`${API_URL}${API_REGISTER_URL}`, value).then(res => {
            Swal.fire({
                title: 'สมัครสมาชิกสำเร็จ!',
                type: 'success',
                timer: 1500,
                showConfirmButton: false
            })
            this.props.history.push('/auth')
        }).catch(err => {
            if (err.response.data.user_exists) {
                Swal.fire({
                    type: 'error',
                    title: 'มีข้อมูลอยู่ในระบบกรุณาติดต่อเจ้าหน้าที่',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    renderBankFields = ({ fields }) => {
        const {
            banks
        } = this.state

        return fields.map((field, index) => {
            return (
                <div key={index} className='row'>
                    <div className='col'>
                        <Field component={SelectField} label='ธนาคาร' name={`${field}.bank`} options={banks}
                            getOptionLabel={this.getOptionBankLabel} getOptionValue={this.getOptionValue} validate={required()} />
                    </div>
                    <div className='col'>
                        <Field component={InputField} label='เลขที่บัญชีธนาคาร' name={`${field}.bank_no`} validate={required()} />
                    </div>
                    <div className='col-1 d-flex justify-content-center align-items-center'>
                        {index !== 0 &&
                        <Button color='link' className='text-danger' onClick={() => fields.remove(index)}>
                            <ClearIcon />
                        </Button>
                        }
                    </div>
                </div>
            )
        })
    }

    getOptionValue = option => option.id
    getOptionBankLabel = option => <div>{getBankIcon(option.code)} {option.name}</div>

    render () {
        const {
            classes,
            pristine,
            submitting,
            handleSubmit
        } = this.props
        return (
            <Container component='main' maxWidth='sm'>
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <PersonOutlineIcon />
                    </Avatar>
                    <Typography component='h1' variant='h5'>สมัครสมาชิก</Typography>
                    <form className={classes.form} onSubmit={handleSubmit(this.submitForm)}>
                        <Field component={InputField} label='username' name='username' autoComplete='username' validate={[required()]} />
                        <Field component={InputField} label='รหัสผ่าน' name='password' type='password' validate={required()} />
                        <Field component={InputField} label='ชื่อ' name='first_name' validate={required()} />
                        <Field component={InputField} label='นามสกุล' name='last_name' validate={required()} />
                        <Field component={InputField} label='อีเมล์' name='email' autoComplete='email' validate={[required(), email()]} />
                        <Field component={InputField} name='telephone' label='หมายเลขโทรศัพท์ที่ติดต่อได้สะดวก' type='text' validate={required()} />
                        <Field component={InputField} name='line_id' label='Line ID' type='text' validate={required()} />
                        {/** **ต้องกรอกข้อมูลธนาคารอย่างน้อย 1 บัญชี */}
                        <FieldArray name='bank_accounts' component={this.renderBankFields} validate={[required(), length({ minimum: 1 })]} />
                        ในการสมัครสมาชิกนี้ ข้าพเจ้าขอยืนยันว่ามีอายุครบ 18 ปีบริบูรณ์ ได้อ่าน และยอมรับข้อกำหนดและเงื่อนไขทั้งหมดของทางเว็บไซต์
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <Button fullWidth variant='contained' className={classes.submit} onClick={this.resetForm}>ยกเลิก</Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button type='submit' color='primary' variant='contained' fullWidth className={classes.submit} disabled={pristine || submitting}>สมัครสมาชิก</Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        )
    }
}

export default reduxForm({
    form: 'register',
    enableReinitialize: true,
    initialValues: {
        bank_accounts: [{}]
    }
})(withStyles(styles)(RegisterView))
