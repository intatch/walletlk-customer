import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { withRouter } from 'react-router-dom'
import { required } from 'redux-form-validators'
import Swal from 'sweetalert2'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'

import { getNoDataTableBody } from '../components/table'
import { STATUS_APPROVED, STATUS_PENDING, STATUS_REJECTED } from '../utils'
import {
    API_GAME_URL,
    API_CUSTOMER_CREDIT_ACCOUNT_URL, API_PROMOTION_URL
} from '../utils/urls'
import { getCustomerCreditAccountStatus } from '../utils/helpers'
import client from '../utils/client'
import SelectField from '../components/forms/form-select'
import ModalFormOpenAccountDeposit from '../components/modals/modal-form-open-account-deposit'

class CasinoView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            games: [],
            promotions: [],
            customerCreditAccounts: [],
            selectedCustomerCreditAccount: null,
            isModalOpen: false
        }
    }

    componentDidMount () {
        this.fetchGames()
        this.fetchPromotions()
        this.fetchCustomerCreditAccounts()
    }

    fetchGames = () => {
        client.get(API_GAME_URL).then(res => {
            this.setState({
                games: res.data
            })
        })
    }

    fetchPromotions = () => {
        client.get(API_PROMOTION_URL).then(res => {
            this.setState({
                promotions: res.data
            })
        })
    }

    fetchCustomerCreditAccounts = () => {
        client.get(API_CUSTOMER_CREDIT_ACCOUNT_URL).then(res => {
            this.setState({
                customerCreditAccounts: res.data
            })
        })
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            selectedCustomerCreditAccount: null
        })
    }

    openDepositModal = (row) => {
        this.setState({
            isModalOpen: true,
            selectedCustomerCreditAccount: row
        })
    }

    alertOpenAccountReqested = () => {
        Swal.fire({
            type: 'success',
            title: 'เปิด USER / ทางการเล่น',
            detail: 'ส่งคำขอเปิดบัญชีแล้ว กรุณาโอนเงินเพื่อยืนยันการขอเปิดบัญชี',
            showConfirmButton: false,
            timer: 1500
        })
    }

    requestCustomerAccount = (customerCreditAccountId) => {
        const {
            customerCreditAccounts
        } = this.state

        Swal.fire({
            title: 'ยืนยันการขอเป็นบัญชีใหม่?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                client.post(API_CUSTOMER_CREDIT_ACCOUNT_URL, { customer_credit_account_id: customerCreditAccountId }).then(res => {
                    this.alertOpenAccountReqested()
                    this.setState({
                        customerCreditAccounts: customerCreditAccounts.map(row => row.id === customerCreditAccountId ? res.data : row)
                    })
                })
            }
        })
    }

    submitForm = (value) => {
        const {
            games,
            customerCreditAccounts
        } = this.state

        client.post(API_CUSTOMER_CREDIT_ACCOUNT_URL, value).then(res => {
            this.alertOpenAccountReqested()
            this.props.reset()
            this.setState({
                games: games.filter(row => row.id !== value.game.id),
                customerCreditAccounts: [...customerCreditAccounts, res.data]
            })
        })
    }

    getUsernameCell = (row) => {
        let username = '-'
        if (row.status === STATUS_APPROVED) {
            username = row.username
        } else if (row.status === STATUS_PENDING) {
            username = <Button color='primary' variant='contained' fullWidth onClick={() => this.openDepositModal(row)}>ฝากเงิน</Button>
        } else if (row.status === STATUS_REJECTED) {
            username = <Button color='primary' variant='contained' fullWidth onClick={() => this.requestCustomerAccount(row.id)}>ขอเปิดบัญชีใหม่</Button>
        }
        return username
    }

    getTableBody = () => {
        const {
            customerCreditAccounts
        } = this.state

        if (customerCreditAccounts.length === 0) {
            return getNoDataTableBody(6)
        }

        return customerCreditAccounts.map((row, index) => (
            <TableRow key={index}>
                <TableCell>{row.game.name}</TableCell>
                <TableCell>{this.getUsernameCell(row)}</TableCell>
                <TableCell>{row.status === STATUS_APPROVED && row.password}</TableCell>
                <TableCell>{row.promotion ? row.promotion.name : '-'}</TableCell>
                <TableCell>{getCustomerCreditAccountStatus(row.status)}</TableCell>
            </TableRow>
        ))
    }

    getOptionLabel = option => option.name
    getOptionValue = option => option.id

    render () {
        const {
            handleSubmit
        } = this.props

        const {
            games,
            promotions,
            isModalOpen,
            selectedCustomerCreditAccount
        } = this.state
        return (
            <Container component='main' className='mt-5 align-self-center' maxWidth='md'>
                <Grid container alignItems='center' justify='center'>
                    <Typography component='h1' variant='h5'>เปิด USER / ทางการเล่น</Typography>
                </Grid>
                <form onSubmit={handleSubmit(this.submitForm)}>
                    <Grid container spacing={2} alignItems='center'>
                        <Grid item xs={4}>
                            <Field component={SelectField} label='เลือกคาสิโน' name='game' options={games}
                                getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} validate={required()} />
                        </Grid>
                        <Grid item xs={4}>
                            <Field component={SelectField} label='เลือกโปรโมชั่น' name='promotion' options={promotions}
                                getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} isClearable />
                        </Grid>
                        <Grid item xs={2}>
                            <Button type='submit' color='primary' variant='contained' fullWidth>เปิดบัญชี</Button>
                        </Grid>
                    </Grid>
                </form>
                <Grid justify='center'>
                    <Paper className='overflow-auto mt-3'>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Casino</TableCell>
                                    <TableCell>Username</TableCell>
                                    <TableCell>Password</TableCell>
                                    <TableCell>โปรโมชั่น</TableCell>
                                    <TableCell>สถานะ</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </Grid>
                <ModalFormOpenAccountDeposit isModalOpen={isModalOpen} toggle={this.toggleModal} customerCreditAccount={selectedCustomerCreditAccount} />
            </Container>
        )
    }
}

const reduxFormCasino = reduxForm({
    form: 'casino',
    enableReinitialize: true
})(CasinoView)

export default withRouter(reduxFormCasino)
