import React, { Component } from 'react'
import { connect } from 'react-redux'
import { authLogout } from '../actions/authentication'
import { Link, withRouter } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import Line from '../../assets/images/line.png'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import client from '../utils/client'
import { API_SETTING_URL } from '../utils/urls'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import routes from '../utils/routes'
import { withStyles } from '@material-ui/core'

const styles = theme => ({
    root: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    },
    rootCard: {
        padding: theme.spacing(3, 2),
        marginTop: '40px'
    },
    card: {
        textAlign: 'center'
    },
    media: {
        background: 'linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%)'
    }
})

const iconStyle = {
    width: '150px',
    height: '150px',
    color: '#fff'
}

class HomeView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            lineId: '-',
            lineQrCodeUrl: '#'
        }
    }

    componentDidMount () {
        this.fetchSettings()
    }

    fetchSettings = () => {
        client.get(API_SETTING_URL).then(res => {
            this.setState({
                lineId: res.data.line_id || '-',
                lineQrCodeUrl: res.data.line_qr_code_url || '#'
            })
        })
    }

    renderCardMenu = (item, onClick) => {
        const {
            classes
        } = this.props
        return (
            <Card className={classes.card} onClick={onClick}>
                <CardContent className={classes.media}>{item.icon}</CardContent>
                <CardContent>
                    <Typography gutterBottom variant='h5' component='h2'>
                        {item.name}
                    </Typography>
                </CardContent>
            </Card>)
    }

    renderCardRouteList = () => {
        return routes.filter(item => item.isShowOnMenu).map(item => (
            <Grid key={item.name} item xs={12} sm={4} className='p-2'>
                <Link to={item.path}>
                    {this.renderCardMenu(item)}
                </Link>
            </Grid>
        ))
    }

    renderCardOtherList = () => {
        const menuList = [{
            name: `Line ID: ${this.state.lineId}`,
            icon: <img src={Line} width={150} height={150} />,
            onClick: () => window.open(this.state.lineQrCodeUrl)
        }, {
            name: 'ออกจากระบบ',
            icon: <ExitToAppIcon style={iconStyle} />,
            onClick: this.props.authLogout
        }]

        return menuList.map(item => (
            <Grid key={item.name} item xs={12} sm={4} className='p-2'>
                {this.renderCardMenu(item, item.onClick)}
            </Grid>
        ))
    }

    render () {
        const {
            classes
        } = this.props
        return (
            <div className={classes.root}>
                <Grid container style={{ width: '99%' }} className='m-2'>
                    {this.renderCardRouteList()}
                    {this.renderCardOtherList()}
                </Grid>
            </div>
        )
    }
}

function mapStateToProps ({ auth }) {
    return {
        auth
    }
}

export default withRouter(
    connect(mapStateToProps, { authLogout })(withStyles(styles)(HomeView))
)
