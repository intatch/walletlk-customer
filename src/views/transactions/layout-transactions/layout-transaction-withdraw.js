import React, { Component } from 'react'
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import FormTransactionWithdraw from '../../../components/form-layouts/form-layout-withdraw'
import { withStyles } from '@material-ui/core'
import { connect } from 'react-redux'

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper
    },
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column'
    }
})

class LayoutTransactionWithdraw extends Component {
    render () {
        const {
            classes,
            customerCreditAccounts
        } = this.props
        return (

            <Container component='main' maxWidth='md'>
                <CssBaseline />

                <div className={classes.paper}>
                    <Grid container alignItems='center' justify='center'>
                        <Typography component='h1' variant='h5'>ถอนเงิน</Typography>
                    </Grid>
                    <Box>การจำกัดยอดขั้นต่ำ : 500</Box>
                    <Box>การจำกัดยอดเงินต่อวัน : ไม่จำกัด</Box>
                    <Box>
                        <Typography color='secondary' variant='h6' gutterBottom>
                            ฟรีค่าธรรมเนียมในการถอน ธนาคาร กสิกร ไทยพาณิชย์ กรุงไทย
                            ครั้งแรกต่อวัน ครั้งต่อไปถ้ามีค่าธรรมเนียมหักจากลูกค้าตามจริง
                            ส่วนธนาคารอื่น ๆ คิดค่าธรรมเนียมตามจริง
                        </Typography>
                    </Box>
                    <FormTransactionWithdraw customerCreditAccounts={customerCreditAccounts} />
                </div>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    const {
        global: {
            customerCreditAccounts
        }
    } = state
    return {
        customerCreditAccounts
    }
}

export default connect(mapStateToProps)(
    withStyles(styles)(LayoutTransactionWithdraw)
)
