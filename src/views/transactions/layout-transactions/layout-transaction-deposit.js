import React, { Component } from 'react'
import Swal from 'sweetalert2'

import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'

import FormTransactionDeposit from '../../../components/form-layouts/form-layout-deposit'
import { withStyles } from '@material-ui/core'
import { connect } from 'react-redux'

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        overflowX: 'auto'
    },
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    }
})

class LayoutTransactionDeposit extends Component {
    componentDidMount () {
        Swal.fire({
            title: 'แจ้งเตือน',
            text: 'ลูกค้าต้องโอนเงินมาจากบัญชีที่จะถอนเท่านั้น ไม่งั้นทางเราจะไม่โอนเงินกลับให้นะครับ เนื่องจากมีมิจฉาชีพหลอกโอนเงินลูกค้าโปรดตรวจสอบเลขบัญชีก่อนโอนทุกครั้งครับ',
            type: 'warning'
        })
    }

    render () {
        const {
            classes,
            customerCreditAccounts
        } = this.props
        return (
            <Container component='main' maxWidth='md'>
                <CssBaseline />
                <div className={classes.paper}>
                    <Box mt={1}>
                        <Grid container alignItems='center' justify='center'>
                            <Typography component='h1' variant='h5'>ฝากเงิน</Typography>
                        </Grid>
                        <Typography color='secondary' variant='h6' gutterBottom>
                            หมายเหตุ : ลูกค้าต้องโอนเงินมาจากบัญชีที่จะถอนเท่านั้น
                            ไม่งั้นทางเราจะไม่โอนเงินกลับให้นะครับ
                            เนื่องจากมีมิจฉาชีพหลอกโอนเงินลูกค้า
                            โปรดตรวจสอบเลขบัญชีก่อนโอนทุกครั้งครับ
                        </Typography>
                    </Box>
                    <FormTransactionDeposit customerCreditAccounts={customerCreditAccounts} />
                </div>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    const {
        global: {
            customerCreditAccounts
        }
    } = state
    return {
        customerCreditAccounts
    }
}

export default connect(mapStateToProps)(
    withStyles(styles)(LayoutTransactionDeposit)
)
