import React, { Component } from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import LayoutTransactionDeposit from './layout-transactions/layout-transaction-deposit'
import LayoutTransactionWithdraw from './layout-transactions/layout-transaction-withdraw'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { connect } from 'react-redux'
import { fetchCustomerCreditAccounts } from '../../actions/global'

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper
    },
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
})

class TransactionView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            tabIndex: 'deposit'
        }
    }

    componentDidMount () {
        this.props.fetchCustomerCreditAccounts()
    }

    onChangeTab = (event, tabIndex) => {
        this.setState({ tabIndex })
    }

    render () {
        const {
            classes
        } = this.props

        const {
            tabIndex
        } = this.state
        return (
            <div className={classes.root}>
                <AppBar position='static' style={{ backgroundColor: 'rgb(13, 121, 114)' }}>
                    <Tabs variant='fullWidth' value={tabIndex} onChange={this.onChangeTab}>
                        <Tab value='deposit' label='ฝากเงิน' />
                        <Tab value='withdraw' label='ถอนเงิน' />
                    </Tabs>
                </AppBar>
                {tabIndex === 'deposit' && <LayoutTransactionDeposit />}
                {tabIndex === 'withdraw' && <LayoutTransactionWithdraw />}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {}
}

export default withRouter(
    connect(mapStateToProps, { fetchCustomerCreditAccounts })(
        withStyles(styles)(TransactionView)
    )
)
