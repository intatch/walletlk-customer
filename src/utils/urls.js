export const APP_URL = process.env.MIX_APP_URL || 'http://localhost:8002'
export const API_URL = process.env.MIX_API_URL || 'http://localhost:8000'

export const API_AUTH_URL = `/api/v2/auth/`
export const API_REGISTER_URL = `/api/v2/register/`
export const API_AUTH_REFRESH_TOKEN_URL = `/api/v2/auth/refresh/`

export const API_TRANSACTION_URL = `/api/v2/transactions/`
export const API_TRANSACTION_DETAIL_URL = `/api/v2/transactions/transactionId/`
export const API_TRANSACTION_BANK_URL = `/api/v2/transactions/banks/`
export const API_TRANSACTION_CREDIT_URL = `/api/v2/transactions/credits/`

export const API_CUSTOMER_URL = `/api/v2/customers/`
export const API_CUSTOMER_DETAIL_URL = `/api/v2/customers/customerId/`

export const API_USER_URL = `/api/v2/users/`
export const API_USER_DETAIL_URL = `/api/v2/users/userId/`
export const API_USER_CHANGE_PASSWORD_URL = `/api/v2/users/userId/changepassword/`

export const API_GAME_URL = `/api/v2/games/`
export const API_GAME_DETAIL_URL = `/api/v2/games/gameId/`

export const API_BANK_URL = `/api/v2/banks/`

export const API_SYSTEM_BANK_ACCOUNT_URL = `/api/v2/systembankaccounts/`
export const API_SYSTEM_ACCOUNT_DETAIL_URL = `/api/v2/systemaccounts/systemAccountId/`

export const API_CUSTOMER_CREDIT_ACCOUNT_URL = `/api/v2/customercreditaccounts/`
export const API_CUSTOMER_BANK_ACCOUNT_URL = `/api/v2/customerbankaccounts/`
export const API_PROMOTION_URL = `/api/v2/promotions/`

export const API_TRANSACTION_DEPOSIT_URL = `/api/v2/transactions/deposit/`
export const API_TRANSACTION_WITHDRAW_URL = `/api/v2/transactions/withdraw/`

export const API_SETTING_URL = `/api/v2/settings/`
