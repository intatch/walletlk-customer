import React from 'react'

import CardIcon from '../../assets/images/cards.svg'
import CardGiftCardIcon from '@material-ui/icons/CardGiftcard'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'

import HomeView from '../views/home'
import CasinoView from '../views/casino'
import TransactionView from '../views/transactions/transaction'
import PromotionView from '../views/promotion'

const iconStyle = {
    width: '150px',
    height: '150px',
    color: '#fff'
}

const routes = [{
    path: '/',
    isShowOnMenu: false,
    component: HomeView
}, {
    ordering: 1,
    path: '/casino-accounts',
    name: 'เปิด USER / ช่องทางการเล่น',
    icon: <CardIcon fill='#fff' width={150} height={150} />,
    isShowOnMenu: true,
    component: CasinoView
}, {
    ordering: 2,
    path: '/transactions',
    name: 'ฝาก / ถอนเงิน',
    icon: <AttachMoneyIcon style={iconStyle} />,
    isShowOnMenu: true,
    component: TransactionView
}, {
    ordering: 3,
    path: '/promotions',
    name: 'โปรโมชั่น',
    icon: <CardGiftCardIcon style={iconStyle} />,
    isShowOnMenu: true,
    component: PromotionView
}]

export default routes
