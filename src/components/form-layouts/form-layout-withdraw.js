import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { required } from 'redux-form-validators'

import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

import SelectField from '../forms/form-select'
import InputField from '../forms/form-input'
import { API_TRANSACTION_WITHDRAW_URL } from '../../utils/urls'
import client from '../../utils/client'
import Swal from 'sweetalert2'
import { getBankIcon } from '../../utils/helpers'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core'

class FormTransactionWithdraw extends Component {
    submitForm = (value) => {
        const data = {
            customer_credit_account_id: value.customer_credit_account.id,
            customer_bank_account_id: value.customer_bank_account.id,
            amount: value.amount
        }
        client.post(API_TRANSACTION_WITHDRAW_URL, data).then(res => {
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            this.props.reset()
        })
    }

    getOptionValue = option => option.id
    getOptionBankLabel = option => <div>{getBankIcon(option.bank.code)} {option.bank.name} - {option.bank_no}</div>
    getOptionCreditLabel = option => <div>[{option.game.name}] {option.username}</div>

    render () {
        const {
            reset,
            handleSubmit,
            customerCreditAccounts,
            customerBankAccounts
        } = this.props

        return (
            <form onSubmit={handleSubmit(this.submitForm)}>
                <Field component={SelectField} label='จากบัญชีเว็บ' name='customer_credit_account' options={customerCreditAccounts}
                    getOptionLabel={this.getOptionCreditLabel} getOptionValue={this.getOptionValue} validate={required()} />
                <Field component={SelectField} label='โอนเข้าธนาคาร' name='customer_bank_account' options={customerBankAccounts}
                    getOptionLabel={this.getOptionBankLabel} getOptionValue={this.getOptionValue} validate={required()} />
                <Field component={InputField} type='number' label='ยอดเงินการถอน' name='amount' validate={required()} />
                <Grid container spacing={2}>
                    <Grid item xs>
                        <Button variant='contained' fullWidth onClick={reset}>ยกเลิก</Button>
                    </Grid>
                    <Grid item xs>
                        <Button type='submit' color='primary' variant='contained' fullWidth>แจ้งถอนเงิน</Button>
                    </Grid>
                </Grid>
            </form>
        )
    }
}

const reduxFormTransactionWithdraw = reduxForm({
    form: 'transactionWithdraw',
    enableReinitialize: true
})(FormTransactionWithdraw)

const mapStateToProps = (state) => {
    const {
        global: {
            customerBankAccounts
        }
    } = state
    return {
        customerBankAccounts
    }
}

export default connect(mapStateToProps)(reduxFormTransactionWithdraw)
