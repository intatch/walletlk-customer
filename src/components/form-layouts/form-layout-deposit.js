import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, getFormValues } from 'redux-form'
import { required, numericality } from 'redux-form-validators'
import Swal from 'sweetalert2'

import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'

import InputField from '../forms/form-input'
import SelectField from '../forms/form-select'
import TextareaField from '../forms/form-textarea'
import InputDatePickerField from '../forms/form-date-picker'

import { API_TRANSACTION_DEPOSIT_URL } from '../../utils/urls'
import { getBankIcon, thousandFormat } from '../../utils/helpers'
import client from '../../utils/client'
import moment from 'moment'
import uuidV4 from 'uuid/v4'
import InputFileField from '../forms/form-input-file'

class FormTransactionDeposit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            date: null,
            time: null,
            slipUploadKey: uuidV4()
        }
    }

    resetForm = () => {
        const {
            resetFormCallback
        } = this.props

        this.props.reset()
        this.clearFileInput()
        this.setState({
            date: null,
            time: null
        })
        if (resetFormCallback) resetFormCallback()
    }

    clearFileInput = () => {
        this.props.change('slip_upload', null)
        this.setState({ slipUploadKey: uuidV4() })
    }

    onDatePickerChange = (input, currentDate, dateFormat) => {
        input.onChange(moment(currentDate).format(dateFormat))
        this.setState({
            [input.name]: currentDate
        })
    }

    submitForm = (value) => {
        const {
            date,
            time
        } = this.state

        const {
            submitFormCallback
        } = this.props
        // eslint-disable-next-line no-undef
        let formData = new FormData()

        const transactionDate = moment(date).format('YYYY-MM-DD')
        const transactionTime = moment(time).format('HH:mm:ss')

        let transactionDateTime = moment(`${transactionDate}T${transactionTime}+07:00`)
        if (transactionDateTime.isValid()) {
            transactionDateTime = transactionDateTime.utc().format('YYYY-MM-DD HH:mm:ss')
        } else {
            transactionDateTime = null
        }

        formData.append('customer_credit_account_id', value.customer_credit_account.id)
        formData.append('system_bank_account_id', value.system_bank_account.id)
        formData.append('amount', value.amount)
        formData.append('transaction_at', transactionDateTime)

        if (value.note) {
            formData.append('note', value.note)
        }
        if (value.slip_upload) {
            formData.append('slip_upload', value.slip_upload)
        }

        client.post(API_TRANSACTION_DEPOSIT_URL, formData, { headers: { 'content-type': 'multipart/form-data' } }).then(res => {
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            this.resetForm()
            if (submitFormCallback) submitFormCallback(res)
        })
    }

    getOptionValue = option => option.id
    getOptionBankLabel = option => <div>{getBankIcon(option.bank.code)} {option.bank.name} - {option.name} {option.account_no}</div>
    getOptionCreditLabel = option => <div>[{option.game.name}] {option.username}</div>

    renderDepositDetail = () => {
        const {
            transactionDepositFormValues
        } = this.props

        const systemBankAccount = transactionDepositFormValues.system_bank_account
        const amount = transactionDepositFormValues.amount
        if (systemBankAccount) {
            return (
                <div>
                    <p>โอนเข้าธนาคาร:  {getBankIcon(systemBankAccount.bank.code)} {systemBankAccount.bank.name}</p>
                    <p>ชื่อ: {systemBankAccount.name}</p>
                    <p>หมายเลขบัญชี: {systemBankAccount.account_no}</p>
                    <p>จำนวนเงิน: {thousandFormat(amount || 0)} บาท</p>
                </div>
            )
        }
        return ''
    }

    render () {
        const {
            date,
            time,
            slipUploadKey
        } = this.state

        const {
            handleSubmit,
            systemBankAccounts,
            customerCreditAccounts
        } = this.props

        return (
            <form className='w-100 pt-1' onSubmit={handleSubmit(this.submitForm)}>
                <Field component={SelectField} label='ฝากเข้าบัญชีเว็บ' name='customer_credit_account' options={customerCreditAccounts}
                    getOptionLabel={this.getOptionCreditLabel} getOptionValue={this.getOptionValue} validate={required()} />
                <Field component={SelectField} label='โอนเข้าธนาคาร' name='system_bank_account' options={systemBankAccounts}
                    getOptionLabel={this.getOptionBankLabel} getOptionValue={this.getOptionValue} validate={required()} />
                <Field component={InputField} type='number' label='ยอดเงินการฝาก' name='amount' validate={[required(), numericality({ '>': 0 })]} />
                <Grid container>
                    <Grid item xs={8}>
                        <Field label='วันที่โอนเงิน' name='date' dateFormat='YYYY-MM-DD'
                            maxDate={new Date()} component={InputDatePickerField}
                            selected={date} onDatePickerChange={this.onDatePickerChange} validate={required()} />
                    </Grid>
                    <Grid item xs={4}>
                        <Field label='เวลาโอนเงิน' name='time' dateFormat='HH:mm'
                            timeIntervals={15} component={InputDatePickerField}
                            selected={time} onDatePickerChange={this.onDatePickerChange}
                            showTimeInput showTimeSelect showTimeSelectOnly validate={required()} />
                    </Grid>
                </Grid>
                <Field component={TextareaField} multiline label='หมายเหตุ' name='note' rows={4} />
                <Grid container>
                    <Grid item xs={6}>
                        <Field key={slipUploadKey} component={InputFileField} label='รูปสลิป' name='slip_upload' accept='image/*' clearFileInput={this.clearFileInput} />
                    </Grid>
                    <Grid item xs={6}>
                        {this.renderDepositDetail()}
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs>
                        <Button variant='contained' fullWidth onClick={this.resetForm}>ยกเลิก</Button>
                    </Grid>
                    <Grid item xs>
                        <Button type='submit' color='primary' variant='contained' fullWidth>แจ้งฝากเงิน</Button>
                    </Grid>
                </Grid>
            </form>
        )
    }
}

const reduxFormTransactionDeposit = reduxForm({
    form: 'transactionDeposit',
    enableReinitialize: true
})(FormTransactionDeposit)

const mapStateToProps = (state) => {
    const {
        global: {
            systemBankAccounts
        }
    } = state

    const transactionDepositFormValues = getFormValues('transactionDeposit')(state) || {}
    return {
        systemBankAccounts,
        transactionDepositFormValues
    }
}

export default connect(mapStateToProps)(reduxFormTransactionDeposit)
