import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Route, Switch, withRouter } from 'react-router-dom'
import Error404View from '../../views/error404'
import routes from '../../utils/routes'
import PermissionRoute from '../mixins/permission-route'
import { fetchCustomerBankAccounts, fetchSystemBankAccounts } from '../../actions/global'

class LayoutAuthenticated extends React.Component {
    componentDidMount () {
        this.props.fetchSystemBankAccounts()
        this.props.fetchCustomerBankAccounts()
    }

    getRoute = (route) => {
        const routePath = route.path
        if (route.isRedirectRoute) {
            return <Redirect key={routePath} exact from={routePath} to={route.to} />
        } else if (route.requiredPermission) {
            return <PermissionRoute key={routePath} exact path={routePath} component={route.component} requiredPermission={route.requiredPermission} />
        }
        return <Route key={routePath} exact path={routePath} component={route.component} />
    }

    getRoutes = () => {
        let routeList = []
        routes.map(route => {
            const subRoutes = route.subRoutes || []
            if (subRoutes.length > 0) {
                subRoutes.map(subRoute => { routeList.push(this.getRoute(subRoute)) })
            } else {
                routeList.push(this.getRoute(route))
            }
        })
        return routeList
    }

    render () {
        return (
            <div id='main-wrapper' >
                <Switch>
                    {this.getRoutes()}
                    <Route component={Error404View} />
                </Switch>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ...state
    }
}

export default withRouter(
    connect(mapStateToProps, {
        fetchSystemBankAccounts,
        fetchCustomerBankAccounts
    })(LayoutAuthenticated)
)
