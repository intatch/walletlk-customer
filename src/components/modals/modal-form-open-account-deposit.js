import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Modal, ModalBody, ModalHeader } from 'reactstrap'
import uuidV4 from 'uuid/v4'
import FormTransactionDeposit from '../form-layouts/form-layout-deposit'

class ModalFormOpenAccountDeposit extends Component {
    constructor (props) {
        super(props)
        this.state = {
            formKey: uuidV4()
        }
    }

    toggle = () => {
        this.props.toggle()
    }

    onClosed = () => {
        this.setState({
            formKey: uuidV4()
        })
    }

    render () {
        const {
            isModalOpen,
            customerCreditAccount
        } = this.props

        const initialValues = {
            customer_credit_account: customerCreditAccount
        }

        return (
            <Modal size='lg' isOpen={isModalOpen} toggle={this.toggle} onClosed={this.onClosed}>
                <ModalHeader>แจ้งฝากเงินเปิดบัญชี</ModalHeader>
                <ModalBody>
                    <FormTransactionDeposit resetFormCallback={this.toggle} initialValues={initialValues} customerCreditAccounts={[customerCreditAccount]} />
                </ModalBody>
            </Modal>
        )
    }
}

export default withRouter(ModalFormOpenAccountDeposit)
