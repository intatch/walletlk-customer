import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Link } from 'react-router-dom'

class NavBar extends React.Component {
    render () {
        return (
            <AppBar style={{ backgroundColor: '#0fb8ad' }} position='static'>
                <Toolbar>
                    <Link style={{ color: '#fff' }} to='/'>
                        <Typography variant='h6' className=''>
                            Wallet LK
                        </Typography>
                    </Link>
                </Toolbar>
            </AppBar>
        )
    }
}

export default NavBar
