import React from 'react'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import { isPermissive } from '../../utils/helpers'
import Error403View from '../../views/error403'

const PermissionRoute = ({ component: Component, ...rest }) => {
    const {
        auth,
        requiredPermission
    } = rest

    const getRenderComponent = (props) => {
        return isPermissive(auth, requiredPermission) ? <Component {...props} /> : <Error403View />
    }

    return <Route {...rest} render={getRenderComponent} />
}

function mapStateToProps ({ auth }) {
    return {
        auth
    }
}

export default connect(mapStateToProps)(PermissionRoute)
