import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux'

const AuthorizedRoute = ({ component: Component, ...rest }) => {
    const getRenderComponent = (props) => {
        return window.localStorage.getItem('refreshToken') ? <Component {...props} /> : <Redirect to='/auth' />
    }

    return <Route {...rest} render={getRenderComponent} />
}

const mapStateToProps = ({ auth }) => {
    return {
        auth
    }
}

export default connect(mapStateToProps)(AuthorizedRoute)
