import React, { Component } from 'react'
import { Button, FormFeedback, FormGroup, Input, Label } from 'reactstrap'
import ClearIcon from '@material-ui/icons/Clear'

class InputFileField extends Component {
    adaptFileEventToValue = delegate => e => delegate(e.target.files[0])

    clearInput = () => {
        const {
            clearFileInput
        } = this.props
        if (clearFileInput) clearFileInput()
    }

    render () {
        const {
            label,
            input: {
                value,
                onChange,
                onBlur,
                ...inputProps
            },
            meta: {
                touched,
                error
            },
            ...props
        } = this.props
        const isError = (touched && error)
        return (
            <FormGroup >
                {label && <Label for={`${inputProps.name}-${label}`}>{label}</Label>}
                <div className='row'>
                    <div className='col'>
                        <Input
                            {...inputProps}
                            {...props}
                            type='file'
                            id={`${inputProps.name}-${label}`}
                            onChange={this.adaptFileEventToValue(onChange)}
                            onBlur={this.adaptFileEventToValue(onBlur)}
                            invalid={isError} />
                    </div>
                    {value &&
                    <div className='col-auto'>
                        <Button color='link' className='text-danger p-0' onClick={this.clearInput}>
                            <ClearIcon />
                        </Button>
                    </div>
                    }
                </div>
                {isError && <FormFeedback>{error}</FormFeedback>}
            </FormGroup>
        )
    }
}

export default InputFileField
