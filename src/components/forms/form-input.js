import React, { Component } from 'react'
import { FormFeedback, FormGroup, Input, Label } from 'reactstrap'

class InputField extends Component {
    render () {
        const {
            input,
            label,
            meta: {
                touched,
                error
            }
        } = this.props
        const isError = (touched && error)
        return (
            <FormGroup >
                {label && <Label for={`${input.name}-${label}`}>{label}</Label>}
                <Input {...input} {...this.props} id={`${input.name}-${label}`} invalid={isError} />
                {isError && <FormFeedback>{error}</FormFeedback>}
            </FormGroup>
        )
    }
}

export default InputField
