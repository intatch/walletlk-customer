import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import { FormFeedback, FormGroup, Label } from 'reactstrap'

export default class InputDatePickerField extends Component {
    componentDidMount () {
        if (this.props.selected) {
            this.onDatePickerChange(this.props.selected)
        }
    }

    onDatePickerChange = (currentDate) => {
        const {
            input,
            dateFormat,
            onDatePickerChange
        } = this.props
        onDatePickerChange(input, currentDate, dateFormat)
    }

    render () {
        const {
            input,
            label,
            meta: {
                touched,
                error
            }
        } = this.props
        const isError = (touched && error)
        return (
            <FormGroup>
                {label && <Label for={`${input.name}-${label}`}>{label}</Label>}
                <DatePicker
                    {...input}
                    {...this.props}
                    id={`${input.name}-${label}`}
                    className={`form-control ${isError ? 'border border-danger' : ''}`}
                    onChange={this.onDatePickerChange} />
                {isError && <FormFeedback className='d-block' invalid>{error}</FormFeedback>}
            </FormGroup>
        )
    }
}
