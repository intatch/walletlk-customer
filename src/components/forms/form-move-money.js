
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import { Field, reduxForm } from 'redux-form'
import { required } from 'redux-form-validators'
import SelectField from './form-select'
import InputField from './form-input'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper
    },
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}))

function MoveMoneyForm (props) {
    const classes = useStyles()
    const { customerCreditAccouts } = props
    return (
        <form className={classes.form} noValidate>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Field component={SelectField} options={customerCreditAccouts} label='จากบัญชีเว็บ' name='bank' validate={required()} />
                </Grid>
                <Grid item xs={12}>
                    <Field component={SelectField} options={customerCreditAccouts} label='ไปบัญชีเว็บ' name='game' validate={required()} />
                </Grid>
            </Grid>
            <Grid container>
                <Field component={InputField} label='ยอดเงินการย้าย' name='amount' validate={required()} />
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <Button type='submit' fullWidth variant='contained' className={classes.submit}>ยกเลิก</Button>
                </Grid>
                <Grid item xs>
                    <Button type='submit' fullWidth variant='contained' color='primary' className={classes.submit}>แจ้งย้ายเงิน</Button>
                </Grid>
            </Grid>
        </form>
    )
}

export default reduxForm({
    form: 'withdraw',
    enableReinitialize: true
})(MoveMoneyForm)
