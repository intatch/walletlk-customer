import React, { Component } from 'react'
import { FormFeedback, FormGroup, Label } from 'reactstrap'
import Select from 'react-select'

class SelectField extends Component {
    componentDidMount () {
        this.setInputValue()
    }

    componentDidUpdate (prevProps, prevState, snapshot) {
        this.setInputValue()
    }

    setInputValue = () => {
        const {
            input,
            options,
            getOptionValue
        } = this.props

        if (input.value && typeof input.value !== 'object') {
            const objectValue = options.find(option => {
                const optionValue = typeof getOptionValue === 'function' ? getOptionValue(option) : option.value
                return optionValue === input.value
            })
            input.onChange(objectValue)
        }
    }

    render () {
        const {
            input,
            label,
            meta: {
                touched,
                error
            }
        } = this.props
        const isError = (touched && error)
        const customStyles = {
            control: (base, state) => ({
                ...base,
                borderColor: state.isFocused ? '#e9ecef' : isError ? '#fa5838' : '#e9ecef',
                '&:hover': {
                    borderColor: state.isFocused ? '#e9ecef' : isError ? '#fa5838' : '#e9ecef'
                }
            })
        }
        return (
            <FormGroup>
                {label && <Label for={`${input.name}-${label}`}>{label}</Label>}
                <Select
                    {...input}
                    {...this.props}
                    id={`${input.name}-${label}`}
                    styles={customStyles}
                    onBlur={() => input.onBlur(input.value)} />
                {isError && <FormFeedback className='d-block' invalid>{error}</FormFeedback>}
            </FormGroup>
        )
    }
}

export default SelectField
