import {
    GLOBAL_ADD_LOADER,
    GLOBAL_REMOVE_LOADER, GLOBAL_SET_CUSTOMER_BANK_ACCOUNT,
    GLOBAL_SET_CUSTOMER_CREDIT_ACCOUNT,
    GLOBAL_SET_SYSTEM_BANK_ACCOUNT
} from '../utils'
import client from '../utils/client'
import {
    API_CUSTOMER_BANK_ACCOUNT_URL,
    API_CUSTOMER_CREDIT_ACCOUNT_URL,
    API_SYSTEM_BANK_ACCOUNT_URL
} from '../utils/urls'

export function addLoader (requestId) {
    return (dispatch) => {
        dispatch({
            type: GLOBAL_ADD_LOADER,
            payload: requestId
        })
    }
}

export function removeLoader (requestId) {
    return (dispatch) => {
        dispatch({
            type: GLOBAL_REMOVE_LOADER,
            payload: requestId
        })
    }
}

export function fetchSystemBankAccounts (data) {
    return (dispatch) => {
        client.get(API_SYSTEM_BANK_ACCOUNT_URL).then(res => {
            dispatch({
                type: GLOBAL_SET_SYSTEM_BANK_ACCOUNT,
                payload: res.data
            })
        })
    }
}

export function fetchCustomerBankAccounts (data) {
    return (dispatch) => {
        client.get(API_CUSTOMER_BANK_ACCOUNT_URL).then(res => {
            dispatch({
                type: GLOBAL_SET_CUSTOMER_BANK_ACCOUNT,
                payload: res.data
            })
        })
    }
}

export function fetchCustomerCreditAccounts (data) {
    return (dispatch) => {
        client.get(API_CUSTOMER_CREDIT_ACCOUNT_URL, { params: { filterType: 'customerCreditAccount' } }).then(res => {
            dispatch({
                type: GLOBAL_SET_CUSTOMER_CREDIT_ACCOUNT,
                payload: res.data
            })
        })
    }
}
